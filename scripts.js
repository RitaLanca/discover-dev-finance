const Modal = {
    open() {
        //Abrir modal
        //Adicionar class active ao modal
        document
            .querySelector(".modal-overlay")
            .classList.add("active");
    },
    close() {
        //Fechar modal
        //Remover class active do modal
        document
            .querySelector(".modal-overlay")
            .classList.remove("active");
    }
}

const MyStorage = {
    get(){
        return JSON.parse(localStorage.getItem('dev.finances:transactions')) || [];
    },
    set(transactions){
        localStorage.setItem('dev.finances:transactions', JSON.stringify(transactions));
    }

}

const Transaction = {
    all: MyStorage.get(),
    add(transaction) {
        Transaction.all.push(transaction);
        MyStorage.set()
        App.reload()
    },
    remove(index) {
        Transaction.all.splice(index,1);

        App.reload();
    },
    incomes() {
        //somar as entradas
        let income = 0;
        Transaction.all.forEach(t => {
           income = t.amount > 0 ? income + t.amount : income;
        })
        return income;
    },
    expenses() {
        //somar as saidas
        let expense = 0;
        Transaction.all.forEach(t => {
            expense = t.amount < 0 ? expense + t.amount : expense;
        })
        return expense;
    },
    total() {
        return Transaction.incomes() + Transaction.expenses();
    }
}

const DOM = {
    transactionContainer: document.querySelector('#data-table tbody'),
    addTransaction(transaction, index) {
        const tr = document.createElement('tr');
        tr.innerHTML = DOM.innerHTMLTransaction(transaction, index);
        tr.dataset.index = index;
        
        DOM.transactionContainer.appendChild(tr);
    },
    innerHTMLTransaction(transaction, index) {
        const cssClass = transaction.amount > 0 ? 'income' : 'expense';
        
        const amount= Utils.formateCurrency(transaction.amount);
        
        const html = `<td class="description">${transaction.description}</td>
                      <td class="${cssClass}">${amount}</td>
                      <td class="date">${transaction.date}</td>
                      <td><img onclick="Transaction.remove(${index})" src="./assets/minus.svg" alt="Remover Transação"/></td>`;
        return html;
    },
    updateBalance() {
        document.getElementById("incomeDisplay").innerHTML= Utils.formateCurrency(Transaction.incomes());
        document.getElementById("expenseDisplay").innerHTML= Utils.formateCurrency(Transaction.expenses());
        document.getElementById("totalDisplay").innerHTML= Utils.formateCurrency(Transaction.total());
    },
    clearTransactions() {
        DOM.transactionContainer.innerHTML="";
    }
};

const Utils = {
    formateCurrency(value) {
       value= value / 100;
       const amount = Number(value).toLocaleString("pt-PT", {
                    style:"currency",
                    currency:"EUR"
                });
     return amount;
    },
    formatAmount(value) {
        // outra forma: value = Number(value.replace(/\,\./g))*100
        return Number(value) * 100;
    },
    formatDate(date) {
        const splitedDate = date.split("-");
        return `${splitedDate[2]}/${splitedDate[1]}/${splitedDate[0]}}`;
    }
}

const Form = {
    description: document.querySelector('input#description'),
    amount: document.querySelector('input#amount'),
    date: document.querySelector('input#date'),
    getValues() {
        return {
            description: Form.description.value,
            amount: Form.amount.value,
            date: Form.date.value
        }
    },
    validateFields() {
        //ES6 feature: destructing
        const {description,amount,date} = Form.getValues();
        if(description.trim() === "" || amount.trim() === "" || date.trim() === ""){
            throw new Error("Por favor preencha todos os campos");
        }
    },
    formatValues() {
        let {description,amount,date} = Form.getValues();
        amount = Utils.formatAmount(amount);

        date = Utils.formatDate(date);

        return {
            description, 
            amount, 
            date
        }
    },
    clearForm() {
        Form.description.value = "";
        Form.amount.value = "";
        Form.date.value = "";
    },
    saveTransaction(transaction) {
        Transaction.add(transaction);
    },
    submit(event) {
        //interrompe o comportamento padrão que é submter os parametros na querystring
        event.preventDefault();
        try {
            Form.validateFields();
            const transaction = Form.formatValues();
            Form.saveTransaction(transaction);

            //limpar form
            Form.clearForm();

            Modal.close();
        }catch(error)
        {
            alert(error.message);
        }

    }
}

const App = {
    init() {
        Transaction.all.forEach((transaction,index) => {
            DOM.addTransaction(transaction, index);
        })
       
        DOM.updateBalance();

        MyStorage.set(Transaction.all);
    },
    reload() {
        DOM.clearTransactions();
        App.init();
    }
}

App.init();

// Transaction.add({
//     id:5,
//     description:'ALo',
//     amount:-60000,
//     date:'23/01/2021'

// })
